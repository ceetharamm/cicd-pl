# GitLab CI/CD Pipeline with NodeJS Application

Install Nodejs and NPM for this application
https://nodejs.org/en/download


1. Create blank project
  Project name: cicd-pl
  Project deployment target (optional): No deployment planned
  Visibility Level: Public
  CREATE PROJECT
2. Create New file `.gitlab-ci.yml`
  ```
  build_job: 
    script:
        - echo "hello from GitLab CI/CD Example"
  ```
  Commit Message: Add CI-CD File
  COMMIT CHANGES
3. Go to Pipeline under Build and see the Pipeline status
  Click on Passed and See Jobs tab and click on Job ID (#6679746749: build_job) to see more information
  Click in retry to re-run the job again
  

C:\git\scripts\devops\PROJECTS\01-gitlab-cicd\nodejsapplication\app.js
```
const express = require('express')
const app = express()
const port = 8080

app.get('/', (req, res) => {
  res.send('This is my Node application for CICD!!')
})

app.listen(port, () => {
  console.log(`Application is listening at http://localhost:${port}`)
})

```
C:\git\scripts\devops\PROJECTS\01-gitlab-cicd\nodejsapplication\package.json
```
{
    "dependencies": {
        "express": "^4.16.1"
    }
}

```

### Run application 
```
cd C:\git\scripts\devops\PROJECTS\01-gitlab-cicd\nodejsapplication>
npm install
node app.js
```
Browse with http://localhost:8080

### Lets go to Ci/CD Pipeline
update .gitlab-ci.yml in local with below changes
```
build:
  script:
    - apt update -y
    - apt install npm -y
    - npm install
	
deploy:
  script:
    - apt update -y
    - apt install nodejs -y
    - node app.js 
```

### Setup Git Connectivity
```
git remote set-url origin https://gitlab.com/ceetharamm/cicd-pl.git
git remote -v
git branch
git branch feature
git branch
git add .
git status
git commit -m "Updated .gitlab-ci.yml file"
git push origion feature
```

#### This job got failed due to dependency. let us create dependency by using stages
Update .gitlab-ci.yml file
```
stages:
  - build_stage
  - deploy_stage

build:
  stage: build_stage
  script:
    - apt update -y
    - apt install npm -y
    - npm install

deploy:
  stage: deploy_stage
  script:
    - apt update -y
    - apt install nodejs -y
    - node app.js 
```

#### Update artifacts
```
stages:
  - build_stage
  - deploy_stage

build:
  stage: build_stage
  script:
    - apt update -y
    - apt install npm -y
    - npm install
  artifacts:
    paths:
      - node_module
      - package-lock.json

deploy:
  stage: deploy_stage
  script:
    - apt update -y
    - apt install nodejs -y
    - node app.js 
```
Commit Message: Update artifacts in .gitlab-ci.yml file

#### Run App in background also change the image name
```
stages:
  - build_stage
  - deploy_stage

build:
  stage: build_stage
  image: node
  script:
  #  - apt update -y
  #  - apt install npm -y
    - npm install
  artifacts:
    paths:
      - node_modules
      - package-lock.json

deploy:
  stage: deploy_stage
  image: node
  script:
  #  - apt update -y
  #  - apt install nodejs -y
    - node app.js > /dev/null 2>&1 &

```
Commit Message: Update app to run in background in .gitlab-ci.yml file

```
stages:
  - build_stage
  - deploy_stage

build:
  stage: build_stage
  image: node
  script:
#    - apt update -y
#    - apt install npm -y
    - npm install
  artifacts:
    paths:
      - node_modules
      - package-lock.json

deploy:
  stage: deploy_stage
  image: node
  script:
#   - apt update -y
#    - apt install nodejs -y
    - node app.js > /dev/null 2>&1 &
```